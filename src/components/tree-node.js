System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var TreeNode;
    return {
        setters:[],
        execute: function() {
            TreeNode = (function () {
                function TreeNode(data) {
                    this.expanded = true;
                    this.data = null;
                    if (data !== undefined) {
                        this.data = data;
                    }
                }
                TreeNode.prototype.toggle = function () {
                    this.expanded = !this.expanded;
                };
                Object.defineProperty(TreeNode.prototype, "icon", {
                    get: function () {
                        if (this.expanded) {
                            return '-';
                        }
                        return '+';
                    },
                    enumerable: true,
                    configurable: true
                });
                return TreeNode;
            }());
            exports_1("TreeNode", TreeNode);
        }
    }
});
//# sourceMappingURL=tree-node.js.map
import {TreeVO} from '../model/tree-vo.ts';
import {TreeDataProxy} from '../model/tree-data-proxy.ts';
export class TreeNode {

    expanded:boolean = true;
    data:TreeVO = null;

    constructor(data:TreeVO) {
        if(data !== undefined) {
            this.data = data;
        }
    }

    toggle(){
        this.expanded = !this.expanded;
    }
    get icon():string {
        if(this.expanded){
            return '-';
        }
        return '+';
    }
}

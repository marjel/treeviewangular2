import {Component, Input} from 'angular2/core.d.ts';
import {TreeDataProxy} from './../model/tree-data-proxy';

@Component({
  selector: 'tree-view-component',
  templateUrl: 'tree-view-template.html',
  directives: [TreeViewComponent]
})

export class TreeViewComponent {
  @Input() proxy: TreeDataProxy;
}

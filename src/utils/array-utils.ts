import {TreeVO} from "../model/tree-vo.ts";
export class ArrayUtils {

    static Remove(array:Array<TreeVO>, value) {
        var i = array.indexOf(value);
        return i>-1 ? array.splice(i, 1) : [];
    }
}

"use strict";
var ArrayUtils = (function () {
    function ArrayUtils() {
    }
    ArrayUtils.Remove = function (array, value) {
        var i = array.indexOf(value);
        return i > -1 ? array.splice(i, 1) : [];
    };
    return ArrayUtils;
}());
exports.ArrayUtils = ArrayUtils;
//# sourceMappingURL=array-utils.js.map
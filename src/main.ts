

import {Component} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {TreeViewComponent} from './components/tree-view-component';
import {TreeDataProxy} from './model/tree-data-proxy';

@Component({
  selector: 'main-app',
  directives: [TreeViewComponent],
  template: `
    <tree-view-component [proxy]="proxy.data"></tree-view-component>
  `
})
class Main {

}

bootstrap(Main);

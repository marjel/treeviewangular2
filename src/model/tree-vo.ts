export class TreeVO {

    nodes:Array<TreeVO> = [];
    value;
    private _parent:TreeVO = null;
    private _id:string = '';

    constructor(value, id:string, parent:TreeVO=null) {
        this._id = id;
        if(value !== undefined) {
            this.value = value;
        }
        if(parent !== null) {
            this._parent = parent;
        }
    }
    get id():string {
        return this._id;
    }
    get isRoot(){
        return this._parent === null;
    }
    get isLeaf(){
        return this.nodes.length === 0;
    }
    get parent():TreeVO{
        return this._parent;
    }

}

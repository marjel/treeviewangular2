"use strict";
var TreeVO = (function () {
    function TreeVO(value, id, parent) {
        if (parent === void 0) { parent = null; }
        this.nodes = [];
        this._parent = null;
        this._id = '';
        this._id = id;
        if (value !== undefined) {
            this.value = value;
        }
        if (parent !== null) {
            this._parent = parent;
        }
    }
    Object.defineProperty(TreeVO.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeVO.prototype, "isRoot", {
        get: function () {
            return this._parent === null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeVO.prototype, "isLeaf", {
        get: function () {
            return this.nodes.length === 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeVO.prototype, "parent", {
        get: function () {
            return this._parent;
        },
        enumerable: true,
        configurable: true
    });
    return TreeVO;
}());
exports.TreeVO = TreeVO;
//# sourceMappingURL=tree-vo.js.map
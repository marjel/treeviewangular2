"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var tree_vo_ts_1 = require('./tree-vo.ts');
var array_utils_ts_1 = require('../utils/array-utils.ts');
var core_1 = require('angular2/core');
var TreeDataProxy = (function () {
    function TreeDataProxy() {
        this.idIncrementor = 0;
        this.data = this.VOFactory(0);
    }
    TreeDataProxy.prototype.create = function (node, value) {
        this.data.nodes.push(this.VOFactory(value, node));
    };
    TreeDataProxy.prototype.read = function (node) {
        return node.value;
    };
    TreeDataProxy.prototype.update = function (node, value) {
        if (!node.isLeaf) {
            if (node.value !== value) {
                node.value = value;
            }
        }
    };
    TreeDataProxy.prototype.delete = function (node) {
        if (node.parent !== null) {
            this.removeChildrenFromNode(node.parent);
        }
        else {
            this.removeChildrenFromNode(node);
        }
    };
    TreeDataProxy.prototype.getLeafs = function (node) {
        var a = [];
        for (var _i = 0, _a = node.nodes; _i < _a.length; _i++) {
            var n = _a[_i];
            if (n.isLeaf) {
                a.push(n);
            }
        }
        return a;
    };
    TreeDataProxy.prototype.getNodeById = function (nodeId) {
        for (var _i = 0, _a = this.data.nodes; _i < _a.length; _i++) {
            var node = _a[_i];
            if (node.id === nodeId) {
                return node;
            }
        }
        return null;
    };
    TreeDataProxy.prototype.VOFactory = function (value, parent) {
        if (parent === void 0) { parent = null; }
        return new tree_vo_ts_1.TreeVO(value, (this.idIncrementor++).toString(), parent);
    };
    TreeDataProxy.prototype.removeChildrenFromNode = function (node) {
        var _this = this;
        node.nodes.forEach(function (n) {
            array_utils_ts_1.ArrayUtils.Remove(node.nodes, n);
            _this.removeChildrenFromNode(n);
        });
    };
    TreeDataProxy = __decorate([
        core_1.Injectable()
    ], TreeDataProxy);
    return TreeDataProxy;
}());
exports.TreeDataProxy = TreeDataProxy;
//# sourceMappingURL=tree-data-proxy.js.map
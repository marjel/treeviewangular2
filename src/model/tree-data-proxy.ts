import {TreeVO} from './tree-vo.ts';
import {ArrayUtils} from '../utils/array-utils.ts';
import {Injectable} from 'angular2/core';

@Injectable()
export class TreeDataProxy {

    data:TreeVO;
    idIncrementor:number = 0;

    constructor() {
        this.data = this.VOFactory(0);
    }
    create(node:TreeVO, value:number):void{
        this.data.nodes.push(this.VOFactory(value, node));
    }
    read(node):number {
        return node.value;
    }
    update(node:TreeVO, value:number):void {
        if(!node.isLeaf){
            if(node.value !== value){
                node.value = value;
            }
        }
    }

    delete(node:TreeVO):void {
        if(node.parent !== null) {
            this.removeChildrenFromNode(node.parent);
        } else {
            this.removeChildrenFromNode(node);
        }
    }
    getLeafs(node:TreeVO):Array<TreeVO> {
        var a = [];
        for (var n of node.nodes) {
            if(n.isLeaf){
                a.push(n);
            }
        }
        return a;
    }
     getNodeById(nodeId:string):TreeVO {
         for (var node of this.data.nodes) {
             if(node.id === nodeId){
                 return node;
             }
         }
         return null;
    }
    VOFactory(value, parent:TreeVO=null):TreeVO {
        return new TreeVO(value, (this.idIncrementor++).toString(), parent);
    }
    removeChildrenFromNode(node:TreeVO):void {
        node.nodes.forEach(n => {
            ArrayUtils.Remove(node.nodes, n)
            this.removeChildrenFromNode(n);
        });
    }
}
